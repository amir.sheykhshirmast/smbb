import java.util.ArrayList;

public class Manager {
    private ArrayList<Task> all ;

    public Manager() {
        all=new ArrayList<>() ;
    }
    public void newPerson(int type, String name, int price)
    {
        all.add(new Task(type,name,price)) ;
    }
    public Task get(int i)
    {
        return all.get(i) ;
    }
    public int priceAllDebt()
    {
        int ans=0 ;
        for (Task task:all
             ) {
            if(task.type==1)
                ans+=task.price ;
        }
        return ans;
    }
    public int priceAllCredit()
    {
        int ans=0 ;
        for (Task task:all
                ) {
            if(task.type==2)
                ans+=task.price ;
        }
        return ans;
    }
    public int priceAllPaidDebt()
    {
        int ans=0 ;
        for (Task task:all
                ) {
            if(task.type==1 && task.isPaid())
                ans+=task.price ;
        }
        return ans;
    }
    public int priceAllPaidCredit()
    {
        int ans=0 ;
        for (Task task:all
                ) {
            if(task.type==2 && task.isPaid())
                ans+=task.price ;
        }
        return ans;
    }
    public int numberOfTasks()
    {
        return all.size() ;
    }
    public Task back()
    {
        return get(numberOfTasks()-1) ;
    }
}
