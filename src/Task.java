import java.util.Date;

public class Task {
    int type ;
    Date date ;
    String name ;
    int price ;
    boolean paid ;
    public Task(int type, String name, int price) {
        this.type = type;
        this.name = name;
        this.price = price;
        this.date=new Date() ;
        paid=false ;
    }

    public int getType() {
        return type;
    }

    public int getPrice() {
        return price;
    }
    public void pay()
    {
        paid=true ;
    }

    public boolean isPaid() {
        return paid;
    }

    public Date getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return ((type==1)?"debtor  ":"creditor  " )+"name: "+name+"      "+ date.toString();
    }
}
